#!/bin/bash
if [ -f env ]
then
. ./env
fi
rm -rf tmp product
mkdir tmp product
cd src/queue
CLASSPATH=$(echo jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/queue.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/publish
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/publish.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/remote
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/remote.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/filescan
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/filescan.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/subscribe
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/subscribe.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/hugefiles
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/hugefiles.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/zip
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/zip.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
if [ x != "x$VOLTAGESODIR" -a -d "$VOLTAGESODIR" ]
then
mkdir tmp
cd src/encryption
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar $VOLTAGESODIR/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/encryption.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
fi
mkdir tmp
cd src/filedeposit
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/filedeposit.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/awss3deposit
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/awss3deposit.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/hdfsdeposit
CLASSPATH=$(echo ../../product/queue.jar ../queue/jars/*.jar jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/hdfsdeposit.jar `find * -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/ssasubscribe
CLASSPATH=$(echo jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/ssasubscribe.jar `find . -name '*.class'` || exit 1
cd ..
rm -rf tmp
mkdir tmp
cd src/ssapublish
CLASSPATH=$(echo jars/*.jar | tr ' ' ':') javac -d ../../tmp *.java || exit 1
cd ../../tmp
jar cf ../product/ssapublish.jar `find . -name '*.class'` || exit 1
cd ..
rm -rf tmp
